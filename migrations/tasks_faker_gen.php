<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 14.07.2018
 * Time: 14:47
 */

include __DIR__.'/../vendor/autoload.php';

use app\helpers\ConfigHelper;

ConfigHelper::setDbConfig(require_once __DIR__.'/../config/db.php');
$db = new \app\helpers\DbInit(ConfigHelper::getDbConfig());

$faker = Faker\Factory::create();
for ($i = 1; $i < 100; $i++) {
    $task = \app\models\Task::create([
        'status' => rand(0, 1),
        'username' => $faker->userName,
        'email' => $faker->email,
        'task_message' => $faker->text,
    ]);
}