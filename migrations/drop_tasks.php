<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 14.07.2018
 * Time: 0:33
 */

include __DIR__ . '/../vendor/autoload.php';
use app\helpers\ConfigHelper;
use Illuminate\Database\Capsule\Manager as Capsule;

ConfigHelper::setDbConfig(require_once __DIR__ . '/../config/db.php');

$db = new \app\helpers\DbInit(ConfigHelper::getDbConfig());

Capsule::schema()->drop('tasks');