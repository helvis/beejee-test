<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 14.07.2018
 * Time: 0:02
 */

include __DIR__ . '/../vendor/autoload.php';
use app\helpers\ConfigHelper;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

ConfigHelper::setDbConfig(require_once __DIR__ . '/../config/db.php');
$db = new \app\helpers\DbInit(ConfigHelper::getDbConfig());

Capsule::schema()->create('tasks', function(Blueprint $table) {
    $table->increments('id');
    $table->smallInteger('status')->default(0);
    $table->string('username');
    $table->string('email');
    $table->text('task_message');
    $table->string('image')->nullable();
    $table->timestamps();
});