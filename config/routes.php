<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 11.07.18
 * Time: 14:54
 */

use Illuminate\Routing\Router;

/** @var $router Router */

$router->get('/', '\app\controllers\MainController@index');
$router->get('/create', '\app\controllers\MainController@create');
$router->post('/store', '\app\controllers\MainController@store')->middleware([]);