<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 13.07.2018
 * Time: 22:42
 */

return [
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'project',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
];