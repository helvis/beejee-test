<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 15.07.2018
 * Time: 14:27
 */

namespace app\services;

use app\helpers\ConfigHelper;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManager;

class ImageService
{
    public function saveImage(UploadedFile $image)
    {
        $filename = sprintf('%s_%s', time(), $image->getClientOriginalName());

        $imageManager = new ImageManager(['driver' => 'imagick']);
        $resized = $imageManager->make($image->getRealPath());
        $resized->resize(320, 240);
        $resized->save(ConfigHelper::getProjectDir() . '/web/img/' . $filename);

        return $filename;
    }
}