<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 13.07.2018
 * Time: 22:43
 */

namespace app\helpers;

use Phroute\Phroute\RouteCollector;

class ConfigHelper
{
    const CONFIG_FILES_DIR = 'config';
    /**
     * @var array
     */
    private static $dbConfig;

    /**
     * @var RouteCollector
     */
    private static $routes;

    /**
     * @var string
     */
    private static $projectDir;

    /**
     * @var array
     */
    private static $viewsConfig;

    public static function getDbConfig(): array
    {
        return self::$dbConfig;
    }

    public static function setDbConfig(array $dbConfig): void
    {
        self::$dbConfig = $dbConfig;
    }
    
    public static function getRoutes(): RouteCollector
    {
        return self::$routes;
    }

    public static function setRoutes(RouteCollector $routes): void
    {
        self::$routes = $routes;
    }

    public static function getProjectDir(): string
    {
        return self::$projectDir;
    }

    public static function setProjectDir(string $projectDir): void
    {
        self::$projectDir = $projectDir;
    }

    public static function getViewsConfig(): array
    {
        return self::$viewsConfig;
    }

    public static function setViewsConfig(array $viewsConfig)
    {
        self::$viewsConfig = $viewsConfig;
    }
}