<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 14.07.2018
 * Time: 12:47
 */

namespace app\helpers;

class ViewerHelper
{
    private $twig;

    public function __construct()
    {
        $config = ConfigHelper::getViewsConfig();
        $loader = new \Twig_Loader_Filesystem(ConfigHelper::getProjectDir() . DIRECTORY_SEPARATOR . $config['path']);
        $this->twig = new \Twig_Environment($loader);
    }

    /**
     * @param string $templatePath
     * @param array|null $params
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render(string $templatePath, array $params = [])
    {
        return $this->twig->render($templatePath, $params);
    }
}