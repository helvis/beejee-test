<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 14.07.2018
 * Time: 0:41
 */

namespace app\models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 *
 * @property integer $status
 * @property string $username
 * @property string $email
 * @property string task_message
 * @property string image
 *
 * @method static bool create(array $params)
 * @method static Builder last
 * @package app\models
 */
class Task extends Model
{
    const IS_DONE = 1;
    const IS_OPEN = 0;

    protected $guarded = [];

    public function setDone()
    {
        $this->status = self::IS_DONE;
    }

    public function setOpen()
    {
        $this->status = self::IS_OPEN;
    }

    public function scopeLast(Builder $query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}