<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 13.07.18
 * Time: 12:19
 */

namespace app\controllers;

use app\helpers\ViewerHelper;
use app\models\Task;
use app\services\ImageService;
use Illuminate\Http\Request;

class MainController extends Controller
{
    const PER_PAGE = 10;

    private $viewer;

    private $imageService;

    public function __construct(ViewerHelper $viewer, ImageService $imageService)
    {
        $this->viewer = $viewer;
        $this->imageService = $imageService;
    }

    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function index()
    {
        $tasks = Task::last()->paginate(self::PER_PAGE);

        return $this->viewer->render('index.twig', [
            'tasks' => $tasks,
        ]);
    }

    /**
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function create()
    {
        return $this->renderCreate(new Task());
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function store(Request $request)
    {
        $task = new Task;
        $task->fill($request->all());

        $image = $request->file('image');
        if (! is_null($image)) {
            $filename = $this->imageService->saveImage($image);
            $task->image = $filename;
        }

        $task->save();

        return $this->index();
    }

    /**
     * @param \app\models\Task $task
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function renderCreate(Task $task)
    {
        return $this->viewer->render('create.twig', [
            'task' => $task,
        ]);
    }
}