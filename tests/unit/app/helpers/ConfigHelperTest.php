<?php

use app\helpers\ConfigHelper;
use Codeception\Test\Unit;

class ConfigHelperTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
        $dbConfig = [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'project',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ];

        ConfigHelper::setDbConfig($dbConfig);
    }

    protected function _after()
    {
        ConfigHelper::setDbConfig([]);
    }

    public function testGetDbConfig()
    {
        $dbConfig = ConfigHelper::getDbConfig();

        $this->assertTrue(is_array($dbConfig));

        $this->assertEquals('mysql', $dbConfig['driver']);
        $this->assertEquals('localhost', $dbConfig['host']);
        $this->assertEquals('project', $dbConfig['database']);
        $this->assertEquals('root', $dbConfig['username']);
        $this->assertEquals('', $dbConfig['password']);
        $this->assertEquals('utf8', $dbConfig['charset']);
        $this->assertEquals('utf8_unicode_ci', $dbConfig['collation']);
        $this->assertEquals('', $dbConfig['prefix']);
    }
}